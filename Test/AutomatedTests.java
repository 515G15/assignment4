import library.entities.*;
import library.entities.helpers.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.text.SimpleDateFormat;
import java.util.Scanner;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class AutomatedTests {

    private static Scanner scannerInput;
    private static ILibrary library;
    private static ICalendar calendar;
    private static SimpleDateFormat dateFormat;

    private static LibraryFileHelper libraryHelper;
    private static CalendarFileHelper calendarHelper;

    @BeforeEach
    void setUp(){
        libraryHelper = new LibraryFileHelper(new BookHelper(), new PatronHelper(), new LoanHelper());
        calendarHelper = new CalendarFileHelper();

        scannerInput = new Scanner(System.in);
        calendar = calendarHelper.loadCalendar();
        library = libraryHelper.loadLibrary();

        dateFormat = new SimpleDateFormat("dd/MM/yyyy");

        if(library.getPatronList().size() == 0){
            library.addPatron("name", "test", "as", 212342423);
        }
        if (library.getBookList().size() == 0){
            library.addBook("author", "Ti", "2313");
        }
        library.commitLoan(library.issueLoan(library.getBookById(1), library.getPatronById(1)));
    }

    /**
     * this test is for Bug 1. this test tests for the calculation of days.
     * Bug 1: fine is not imposed if one day has passed.
     */

    @Test
    void testFineImposed() {
        calendar.incrementDate(3);
        library.checkCurrentLoansOverDue();
        library.getPatronById(1).incurFine(library.calculateOverDueFine(library.getCurrentLoanByBookId(1)));
        assertEquals(1.0,library.getPatronById(1).getFinesPayable(),0.1);
    }

    /**
     * This test is for Bug 2.  this tests the fine calculated.
     * Bug 2: Fine imposed is half of the expected fine.
     */

    @Test
    void testFineCalculated() {
        calendar.incrementDate(5);
        library.checkCurrentLoansOverDue();
        library.getPatronById(1).incurFine(library.calculateOverDueFine(library.getCurrentLoanByBookId(1)));
        assertEquals(3.0,library.getPatronById(1).getFinesPayable(),0.1);
    }
}

